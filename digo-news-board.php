<?php

/*
Plugin Name: Digo News Board
Plugin URI: http://digo.do
Description: This plugin show latest news from a category.
Version: 1.0
Author: Kevin Jimenez
Author URI: http://URI_Of_The_Plugin_Author
License: GPL2
*/


defined( 'ABSPATH' ) or die( 'Hiiii!!' );

add_action('admin_menu', 'digoNewsBoardMenu');

function digoNewsBoardMenu(){
	add_menu_page(
		'Digo News Board Settings',
		'Digo News Board',
		'manage_options',
		'digo-news-board',
		'digoNewsBoardOptions',
		null,
		5
	);
}

function digoNewsBoardOptions() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
?>

    <h1>Digo News Board Settings</h1>
    <div class='wrap'>
        <p>Configure the news to show</p>
        <div>
            <form name='general-digo-news-board-settings' method='POST' action='options.php'>
                <p>
                    <label for='first'>Category:</label>
                    <input type="text" id='first' class='' placeholder='Category'>
                </p>
                <p>
                    <label for='first'>No. posts to show:</label>
                    <input type="number" id='numberPosts' class='' placeholder='5'>
                </p>
                <?php submit_button(); ?>
            </form>
        </div>
    </div>

<?php

}

require_once 'DigoNewsBoardWidget.php';


