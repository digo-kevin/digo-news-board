<?php
// Template: static_two_columns
$num_large_news= 1;
$first_column_num_news = 2;

if($news_index == 1 || $news_index == $first_column_num_news+1) {
    echo '<div class="col-md-6">';
}
if($news_index <= $num_large_news):
?>
    <div class="main-citizen-journalism-news">
        <h1 class="section-main-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <figure class="section-main-post-picture"><a href="<?php the_permalink(); ?>"><?= the_post_thumbnail('medium'); ?></a></figure>
        <div class="section-main-post-text">
            <p>
                <?= wp_trim_words(get_the_content(), 60); ?>
            </p>
        </div>
        <div class="section-main-post-permalink">
            <a href="<?php the_permalink(); ?>" class="read-more">Leer más</a>
        </div>
        <hr>
    </div>
    <?php else: ?>
    <div class="container-fluid no-padding">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-post-title">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h1>
            </div>
            <div class="col-md-5">
                <figure class="section-post-picture"><a href="<?php the_permalink(); ?>"><?= the_post_thumbnail('thumbnail'); ?></figure>
            </div>
            <div class="col-md-7">
                <div class="section-post-text">
                    <p>
                        <?= wp_trim_words(get_the_content(), 40); ?>
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="section-post-permalink">
                    <a href="<?php the_permalink(); ?>" class="read-more">Leer más</a>
                </div>
            </div>
        </div>
        <hr>
    </div>
<?php endif; ?>
<?php if($news_index == $first_column_num_news || $news_index == $the_news->post_count) echo '</div>'; ?>