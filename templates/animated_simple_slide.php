<?php
// Template: animated_simple_slide
?>

<div class="row news-block">
    <section class="featured-news col-md-12">
        <div class="feature-main-articles">
            <!--main articules-->
            <article class="main-aticle">
                <figure class="main-article-picture">
                    <a href="<?php the_permalink(); ?>"><?= the_post_thumbnail('full'); ?></a>
                    <figcaption></figcaption>
                </figure>
                <div class="highlight-container">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="post-date col-md-6">
                                <p><?php the_date(); ?></p>
                            </div>
                            <div class="read-more col-md-6">
                                <p><a href="<?php the_permalink(); ?>">Leer más... </a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="related-feature-articles">
            <div class="related-articles-title">
                <h2> Noticias relacionadas: </h2>
            </div>
            <div class="related-articles">
                <div class="container">
                    <!--related article 1-->
                    <div class="row">
                        <div class="ra-container col-xs-12 col-sm-6 col-lg-3">
                            <div class="related-article">
                                <h2 class="ra-title">
                                    “En el aire” los afectados por la explosión de planta de gas
                                </h2>
                                <div class="ra-details">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="post-date col-xs-8">
                                                <p>18 de febrero del 2016</p>
                                            </div>
                                            <div class="read-more col-xs-4">
                                                <p><a href="#">Leer más... </a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--related article 2-->
                        <div class="ra-container col-xs-12 col-sm-6 col-lg-3">
                            <div class="related-article">
                                <h2 class="ra-title">
                                    Policía implementará un sistema de huellas dactilares
                                </h2>
                                <div class="ra-details">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="post-date col-xs-8">
                                                <p>18 de febrero del 2016</p>
                                            </div>
                                            <div class="read-more col-xs-4">
                                                <p><a href="#">Leer más... </a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--related article 3-->
                        <div class="ra-container col-xs-12 col-sm-6 col-lg-3">
                            <div class="related-article">
                                <h2 class="ra-title">
                                    Médicos suspenden las protestas para dialogar con  salud</h2>
                                <div class="ra-details">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="post-date col-xs-8">
                                                <p>18 de febrero del 2016</p>
                                            </div>
                                            <div class="read-more col-xs-4">
                                                <p><a href="#">Leer más... </a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--related article 4-->
                        <div class="ra-container col-xs-12 col-sm-6 col-lg-3">
                            <div class="related-article">
                                <h2 class="ra-title">
                                    Industria ordena revisar reglas sobre el GLP
                                </h2>
                                <div class="ra-details">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="post-date col-xs-8">
                                                <p>18 de febrero del 2016</p>
                                            </div>
                                            <div class="read-more col-xs-4">
                                                <p><a href="#">Leer más... </a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>