<?php

/**
 * Class DigoNewsBoardWidget
 */
class DigoNewsBoardWidget extends WP_Widget
{
    /**
     * DigoNewsBoardWidget constructor.
     */
    function __construct()
    {
        // Parameters of Widget ( $id, $plugin_name, $args)
        parent::__construct(
            'digo_news_board',
            'Digo News Board Widget',
            [ 'description' => 'Display news from categories' ]
        );
    }

    /**
     * Function that print the widget front-end
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        wp_enqueue_style('dnb_style', plugins_url('dnbStyle.css', __FILE__));
        wp_enqueue_script('dnb_script', plugins_url('dnbScript.js', __FILE__), null, null, true);

        // Making query with the select category and quantity of posts
        $query_args = [
            "posts_per_page" => $instance['dnb_quantity_posts'],
            "cat" => $instance['dnb_category'],
            "orderby" => "date",
            "order" => "DESC"
        ];
        $the_news = new WP_Query($query_args);
        $news_index = 1;

        // Init print Widget
        echo $args['before_widget'];
        if ( isset($instance['dnb_title']) ) { echo "<h3 class='section-title'>" . $instance['dnb_title'] . "</h3>"; }


        if( $the_news->have_posts() ):
            while( $the_news->have_posts() ):
                $the_news->the_post();

                $file_tmpl = plugin_dir_path( __FILE__ ) . 'templates/' . $instance['dnb_template'] . ".php";

                if( file_exists($file_tmpl) ) {
                    include $file_tmpl;
                } else {
                    echo "No template was found!";
                    break;
                }

                $news_index++;
            endwhile;
        endif;
        // Ending print Widget
        echo $args['after_widget'] ;

        // Reset the data of query
        wp_reset_postdata();
    }

    /**
     * Function that print the widget back-end
     *
     * @param array $instance
     */
    public function form( $instance )
    {
        $default_quantity_posts = 5;
        // Get all categories from database
        $categories = get_terms('category', ['fields'=> 'id=>name']);

        // Map the file template name to template selector in admin side
        // 'template_name' => 'Label in Widget Select'
        $templates = [
            'static_one_column' => 'Static One Column',
            'static_two_columns' => 'Static Two Columns',
            'animated_simple_slide' => 'Animated Simple Slide'
        ];

        // Obtaining data from instance
        $dnb_title = $instance['dnb_title'];
        $dnb_category = $instance['dnb_category'];
        $dnb_quantity_posts = ( !empty($instance['dnb_quantity_posts']) ) ? $instance['dnb_quantity_posts'] : $default_quantity_posts;
        $dnb_template = $instance['dnb_template'];
        ?>
        <p>
            <label for="<?= $this->get_field_id('dnb_title') ?>">Title:</label>
            <input id="<?= $this->get_field_id('dnb_title') ?>" name="<?= $this->get_field_name('dnb_title') ?>" value="<?= $dnb_title ?>" type="text" class="widefat" placeholder="Title">
        </p>
        <p>
            <label for="<?= $this->get_field_id('dnb_category') ?>">Category:</label>
            <select id="<?= $this->get_field_id('dnb_category') ?>" name="<?= $this->get_field_name('dnb_category') ?>">
                <?php foreach ($categories as $cat_id => $cat_name): ?>
                    <option value="<?= $cat_id ?>" <?php if( $dnb_category == $cat_id) echo 'selected' ?>>
                        <?= $cat_name ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?= $this->get_field_id('dnb_quantity_posts') ?>">How many posts:</label>
            <input type="number" class="tiny-text" min="1" name="<?= $this->get_field_name('dnb_quantity_posts') ?>" id="<?= $this->get_field_id('dnb_quantity_posts') ?>" placeholder="5" value="<?= $dnb_quantity_posts ?>">
        </p>
        <p>
            <label for="<?= $this->get_field_id('dnb_template') ?>">Template:</label>
            <select id="<?= $this->get_field_id('dnb_template') ?>" name="<?= $this->get_field_name('dnb_template') ?>">
                <?php foreach ($templates as $template_id => $template_name): ?>
                    <option value="<?= $template_id ?>" <?php if( $dnb_template == $template_id) echo 'selected' ?>>
                        <?= $template_name ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
        <?php
    }

    /**
     * Function that handle the submit from widget back-end
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array $instance
     */
    public function update($new_instance, $old_instance )
    {

        // Verifing data before submit
        $instance = [];
        $instance['dnb_title'] = $new_instance['dnb_title'];
        $instance['dnb_category'] = ( !empty($new_instance['dnb_category']) ) ? $new_instance['dnb_category'] : $old_instance['dnb_category'];
        $instance['dnb_quantity_posts'] = ( $new_instance['dnb_quantity_posts'] >= 1 ) ? $new_instance['dnb_quantity_posts'] : $old_instance['dnb_quantity_posts'];
        $instance['dnb_template'] = ( !empty($new_instance['dnb_template']) ) ? $new_instance['dnb_template'] : $old_instance['dnb_template'];

        return $instance;
    }

}

/**
 *
 */
function registerDNBWidget()
{
    register_widget('DigoNewsBoardWidget');
}

add_action('widgets_init', 'registerDNBWidget');